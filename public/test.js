const PARAMS_DEFAULT = {
    use_string: true,
    min_length: 1,
    max_length: 2048,
    temperature: 0.6,
    top_k: 140,
    top_p: 0.9,
    eos_token_id: 198,
    repetition_penalty: 1.1875,
    repetition_penalty_range: 512,
    repetition_penalty_slope: 6.57,
    tail_free_sampling: 1,
    prefix: "vanilla",
    logit_bias_exp: [
        {
            sequence: [
                1635    // " *" token
            ],
            bias: 0.3,
            ensure_sequence_finish: false,
            generate_once: true
        },
        {
            sequence: [
                9       // "*" token
            ],
            bias: 0.1,
            ensure_sequence_finish: false,
            generate_once: true
        }
    ]
}

const getAccessToken = async (access_key) => {
    const response = await fetch('https://api.novelai.net/user/login', {
        method: 'POST',
        body: JSON.stringify({key: access_key}), // string or object
        headers: {
            'Content-Type': 'application/json',
        }
    });
    const myJson = await response.json(); //extract JSON from the http response
    return myJson.accessToken
}

const generate = async (accessToken, input, model = "euterpe-v2" /*"6B-v4"*/, params = PARAMS_DEFAULT) => {
    const response = await fetch('https://api.novelai.net/ai/generate', {
        method: 'POST',
        body: JSON.stringify({input, model, parameters: params}), // string or object
        headers: {
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + accessToken
        }
    });
    const myJson = await response.json(); //extract JSON from the http response
    return myJson.output
}

async function main() {
    const accessToken = await getAccessToken("YOUR API KEY")
    const prompt = "YOUR PROMPT"
    const result = await generate(accessToken, prompt)
    console.log(prompt + result)
}

main()
