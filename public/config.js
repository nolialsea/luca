let username = localStorage.getItem("username")
if (username === null) {
    username = "User"
}

let novelAiAccessToken = localStorage.getItem("novelAiAccessToken")
const modalIntro = new bootstrap.Modal(document.getElementById('staticBackdrop'), {
    keyboard: false
})
const buttonValidateApiKey = document.getElementById("button-validate-api-key")
if (!novelAiAccessToken) {
    modalIntro.show()
}

buttonValidateApiKey.onclick = async () => {
    const inputNAIApiKey = document.getElementById("inputNAIApiKey")

    const token = await authenticate(inputNAIApiKey.value)
    console.log("token:", token)
    buttonValidateApiKey.classList.remove("btn-secondary")
    buttonValidateApiKey.classList.remove("btn-danger")
    buttonValidateApiKey.classList.remove("btn-success")
    if (token) {
        buttonValidateApiKey.classList.add("btn-success")
        buttonValidateApiKey.innerText = "Valid API key, have fun!"
        localStorage.setItem("novelAiAccessToken", inputNAIApiKey.value)
        setTimeout(() => modalIntro.hide(), 1000)
    } else {
        buttonValidateApiKey.classList.add("btn-danger")
        buttonValidateApiKey.innerText = "Invalid API key, correct it and press here again."
    }
}

const PARAMS_DEFAULT = {
    use_string: true,
    min_length: 1,
    max_length: 2048,
    temperature: 0.6,
    top_k: 140,
    top_p: 0.9,
    eos_token_id: 198,
    repetition_penalty: 1.1875,
    repetition_penalty_range: 512,
    repetition_penalty_slope: 6.57,
    tail_free_sampling: 1,
    prefix: "vanilla",
    logit_bias_exp: [
        {
            sequence: [
                1635
            ],
            bias: 0.3,
            ensure_sequence_finish: false,
            generate_once: true
        },
        {
            sequence: [
                9
            ],
            bias: 0.1,
            ensure_sequence_finish: false,
            generate_once: true
        }
    ]
}

const PARAMS_DEFAULT_GENERATOR = {
    use_string: true,
    min_length: 1,
    max_length: 2048,
    temperature: 0.6,
    top_k: 140,
    top_p: 0.9,
    eos_token_id: 198,
    repetition_penalty: 1,
    tail_free_sampling: 1,
    prefix: "vanilla"
}
