const getAccessToken = async (access_key) => {
    const response = await fetch('/validatekey', {
        method: 'POST',
        body: JSON.stringify({key: access_key}), // string or object
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    });
    const myJson = await response.json(); //extract JSON from the http response
    return myJson?.accessToken
}

async function authenticate(key = novelAiAccessToken) {
    if (key)
        accessToken = await getAccessToken(key)
    return accessToken
}

authenticate().then(r => null)