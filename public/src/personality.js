class AIPersonality {
    id
    name
    description
    context
    img
    messages
    conf

    constructor(name, description, context, img, id = uuidv4(), messages = [], conf = {}) {
        this.id = id
        this.name = name
        this.description = description
        this.context = context
        this.img = img
        this.messages = messages
        this.conf = conf
    }

    static fromJSON(json, isNewPersonality = false) {
        if (!json.name || !json.description || !json.context || !json.img) {
            console.log("invalid json")
            return null
        }

        if (!json.messages) json.messages = []
        if (!json.conf) json.conf = {}

        const messages = []
        for (let message of json.messages) {
            messages.push(Message.fromJSON(message, isNewPersonality))
        }
        return new AIPersonality(json.name, json.description, json.context, json.img, json.id, messages, json.conf)
    }
}


function addPersonality() {
    if (!inputGeneralSettingsNewPersonality.value) return
    const personality = inputGeneralSettingsNewPersonality.value

    if (personality !== null && state !== null) {
        const json = JSON.parse(personality)
        if (json) {
            const newPersonality = AIPersonality.fromJSON(json, true)
            if (newPersonality) {
                state.aiPersonalities.push(newPersonality)
                inbox.append(getInboxEntry(newPersonality))
                alert("New AI Personality created!")
                saveState()
            }
        }
    }
}

function deletePersonality() {
    if (!state || !state.currentAiPersonality) return
    const confirmation = confirm(`Are you sure you want to delete ${state.currentAiPersonality.name}?`)
    if (confirmation) {
        state.aiPersonalities = state.aiPersonalities.filter((ai) => ai.id !== state.currentAiPersonality.id)
        state.load()
        saveState()
    }
}