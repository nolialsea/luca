function htmlToElement(html) {
    const template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

function getDateFromTimestamp(timestamp) {
    const date = new Date(timestamp)
    return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`
}

function getInboxEntry(aiPersonality) {
    return htmlToElement(`
        <a href="#" class="list-group-item list-group-item-action border-0" id="${aiPersonality.id}" onclick="setActiveConversation('${aiPersonality.id}')">
            <div class="badge bg-success float-right" id="${aiPersonality.id}_messageCount">${aiPersonality.messages.length}</div>
            <div class="d-flex align-items-start">
                <img src="${aiPersonality.img}" class="rounded-circle mr-1"
                     alt="${aiPersonality.name}" width="64" height="64">
                <div class="flex-grow-1 ml-3">
                    <strong>${aiPersonality.name}</strong>
                    <div class="small"><span class="fas fa-circle chat-online"></span> ${aiPersonality.description}</div>
                </div>
            </div>
        </a>`
    )
}

function getCaretButton(message) {
    return `
        <button class="btn btn-secondary border btn-sm px-2" onclick="" type="button" data-bs-toggle="collapse" data-bs-target="#collapseActions_${message.id}" aria-expanded="false" aria-controls="collapseExample"
                data-toggle="tooltip" data-placement="top" title="Show actions">
            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-caret-right" viewBox="0 0 16 16">
              <path d="M6 12.796V3.204L11.481 8 6 12.796zm.659.753 5.48-4.796a1 1 0 0 0 0-1.506L6.66 2.451C6.011 1.885 5 2.345 5 3.204v9.592a1 1 0 0 0 1.659.753z"/>
            </svg>
        </button>
        <span class="collapse" id="collapseActions_${message.id}">
            ${getEditMessageButton(message)}
            ${getRetryMessageButton(message)}
            ${getRetryMessageFromInputButton(message)}
            ${getContinueMessageButton(message)}
            ${getReactionMessageButton(message)}
            ${getMemorizeMessageButton(message)}
            ${getDeleteMessageButton(message)}
            ${getDeleteMessagesUpToHereButton(message)}
        </span>`.trim()
}

function getDeleteMessageButton(message) {
    return `
        <button class="btn btn-danger border btn-sm px-2" onclick="deleteMessage('${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Delete this message">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                 class="bi bi-trash" viewBox="0 0 16 16">
                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                <path fill-rule="evenodd"
                      d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
            </svg>
        </button>`.trim()
}

function getDeleteMessagesUpToHereButton(message) {
    return `
        <button class="btn btn-danger border btn-sm px-2" onclick="deleteMessageUpToHere('${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Delete up to here (this message included)">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
              <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
            </svg>
        </button>`.trim()
}

function getEditMessageButton(message) {
    return `
        <button class="btn btn-primary border btn-sm px-2" onclick="editMessage(event, '${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Edit this message">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
              <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
            </svg>
        </button>`.trim()
}

function getRetryMessageButton(message) {
    return `
        <button class="btn btn-primary border btn-sm px-2" onclick="retryMessage(event, '${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Retry this message">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-repeat" viewBox="0 0 16 16">
              <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z"/>
              <path fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z"/>
            </svg>
        </button>`.trim()
}

function getRetryMessageFromInputButton(message) {
    return `
        <button class="btn btn-primary border btn-sm px-2" onclick="retryMessageFromInput(event, '${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Retry this message from last input">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-counterclockwise" viewBox="0 0 16 16">
              <path fill-rule="evenodd" d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"/>
              <path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"/>
            </svg>
        </button>`.trim()
}

function getContinueMessageButton(message) {
    return `
        <button class="btn btn-primary border btn-sm px-2" onclick="continueMessage(event, '${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Continue this message">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                class="bi bi-arrow-right-circle" viewBox="0 0 16 16">
                <path fill-rule="evenodd"
                  d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/>
            </svg>
        </button>`.trim()
}

function getReactionMessageButton(message) {
    return `
        <button class="btn btn-primary border btn-sm px-2" onclick="addReactionToMessage(event, '${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Add a reaction to this message">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-asterisk" viewBox="0 0 16 16">
              <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z"/>
            </svg>
        </button>`.trim()
}

function getMemorizeMessageButton(message) {
    return `
        <button class="btn btn-primary border btn-sm px-2" onclick="memorizeMessages('${message.id}')"
                data-toggle="tooltip" data-placement="top" title="Memorize this message">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cloud-plus" viewBox="0 0 16 16">
              <path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>
              <path d="M4.406 3.342A5.53 5.53 0 0 1 8 2c2.69 0 4.923 2 5.166 4.579C14.758 6.804 16 8.137 16 9.773 16 11.569 14.502 13 12.687 13H3.781C1.708 13 0 11.366 0 9.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383zm.653.757c-.757.653-1.153 1.44-1.153 2.056v.448l-.445.049C2.064 6.805 1 7.952 1 9.318 1 10.785 2.23 12 3.781 12h8.906C13.98 12 15 10.988 15 9.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 4.825 10.328 3 8 3a4.53 4.53 0 0 0-2.941 1.1z"/>
            </svg>
        </button>`.trim()
}

function getHistoryIncomingEntry(aiPersonality, message) {
    return htmlToElement(`
        <div class="chat-message-left pb-4" id="${message.id}">
            <div>
                <img src="${aiPersonality.img}"
                     class="rounded-circle mr-1" alt="${aiPersonality.name}" width="64" height="64">
            </div>
            <div class="flex-shrink-1 bg-light rounded py-2 px-3 ml-3">
                <div class="font-weight-bold mb-1">
                    <strong id="${message.id}_name">${aiPersonality.name}</strong> <span class="text-muted small text-nowrap mt-2">${getDateFromTimestamp(message.timestamp)}</span>
                    ${getCaretButton(message)}
                </div>
                <span id="${message.id}_text">${message.message}</span>
                <span><input id="${message.id}_textInput" style="display: block" class="messageEdit" value="${message.message}" onkeydown="editMessage(event, '${message.id}', true)" hidden /></span>
                <div class="spinner-border spinner-border-sm" role="status" id="${message.id}_spinner" hidden>
                  <span class="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>`
    )
}

function getHistoryOutgoingEntry(message) {
    return htmlToElement(`
        <div class="chat-message-right pb-4" id="${message.id}">
            <div>
                <img src="img/user.png"
                     class="rounded-circle mr-1" alt="You" width="40" height="40">
            </div>
            <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
                <div class="font-weight-bold mb-1">
                    <strong id="${message.id}_name">${username}</strong> <span class="text-muted small text-nowrap mt-2">${getDateFromTimestamp(message.timestamp)}</span>
                    ${getCaretButton(message)}
                </div>
                <span id="${message.id}_text">${message.message}</span>
                <span><input id="${message.id}_textInput" style="display: block" class="messageEdit" value="${message.message}" onkeydown="editMessage(event, '${message.id}', true)" hidden /></span>
                <div class="spinner-border  spinner-border-sm" role="status" id="${message.id}_spinner" hidden>
                  <span class="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>`
    )
}