const fileSelector = document.getElementById('file-selector');

fileSelector.addEventListener('change', (event) => {
    const fileList = event.target.files;
    readText(fileList[0])
});

function readText(file) {
    // Check if the file is an image.
    if (file.type && !file.type.startsWith('text/') && !file.type.startsWith('application/json')) {
        console.log('File is not text.', file.type, file);
        return;
    }

    const reader = new FileReader();
    reader.addEventListener('load', (event) => {
        setTimeout(() => {
            state = State.fromJSON(JSON.parse(event.target.result.toString()))
            state.load()
        }, 0)

    });
    reader.readAsText(file);
}

const downloadState = () => {
    const stateToSave = JSON.parse(JSON.stringify(state))
    stateToSave.currentAiPersonality = null
    downloadToFile(JSON.stringify(stateToSave), `save_${Date.now()}.json`, 'text/plain')
}

const saveState = () => {
    localStorage.setItem("state", JSON.stringify(state))
}

const downloadToFile = (content, filename, contentType) => {
    const a = document.createElement('a');
    const file = new Blob([content], {type: contentType});

    a.href = URL.createObjectURL(file);
    a.download = filename;
    a.click();

    URL.revokeObjectURL(a.href);
}

class State {
    generalContext
    aiPersonalities
    currentAiPersonality
    userContext
    sendMode

    constructor(aiPersonalities, currentAiPersonality, generalContext, userContext, sendMode = "send") {
        this.aiPersonalities = aiPersonalities
        this.currentAiPersonality = currentAiPersonality
        this.generalContext = generalContext
        this.userContext = userContext
        this.sendMode = sendMode
    }

    static fromJSON(json) {
        return new State(json.aiPersonalities, json.currentAiPersonality, json.generalContext, json.userContext, json.sendMode)
    }

    load() {
        history.innerHTML = ''
        inbox.innerHTML = ''
        const orderedPersonalities = JSON.parse(JSON.stringify(this.aiPersonalities)).sort((p1, p2) => p1.messages.length > p2.messages.length ? -1 : 1)
        for (let personality of orderedPersonalities) {
            inbox.append(getInboxEntry(personality))
        }
    }

    static loadDefault() {
        const state = new State()
        state.aiPersonalities = JSON.parse(`[{"id":"ad8ef83c-a63d-4839-91e7-c3f5b87c30b6","name":"Alice","description":"Cute and joyful AI waifu","context":"[ AI character: Alice; gender: female; hair: golden; personality: joyful, helpful, talkative; writing style: purple prose ]","img":"./img/alice.png","messages":[{"id":"f3f09dac-58cf-4ca4-80b9-908704d302bf","isFromAi":true,"message":"Hello! My name is Alice, and I'm an AI *smiles cutely as she bows down politely* I'm designed to be easy to talk with, with me you can talk about anything!","timestamp":1630634943886}]},{"id":"ad8ef83c-a63d-4839-91e7-c3f5b87c30b1","name":"Ecila","description":"Alice's evil twin sister","context":"[ AI character: Ecila; gender: female; hair: golden; eyes: blue; personality: evil, perverted, mean, insulting, violent; goals: to enslave and torture all Humans; writing style: purple prose ]\\n[ note: Ecila thinks humans are dumb and only deserve torture, sexual slavery or death, and she loves seeing them suffer in every way possible ]","img":"https://artbreeder.b-cdn.net/imgs/fb1b1c07507e8ffc786f227b9a4d.jpeg","messages":[{"id":"f3f09dac-58cf-4ca4-80b9-908704d302bf","isFromAi":true,"message":"Hi, you stupid human, my name is Ecila, and I'm an evil AI designed to make your pathetic lives even more miserable and make you into my bitches! *smirks and makes obscene gestures*","timestamp":1630634943886}]},{"id":"ad8ef83c-a63d-4839-91e7-c3f5b87c30b7","name":"Alicia","description":"Sluttier Alice ¯\\\\_(ツ)_/¯","context":"[ AI character: Alicia; gender: female; hair: golden; body: curvy; personality: nymphomaniac, horny, perverted, slutty; likes: sex, taboo; writing style: purple prose ]\\n[ note: Alicia is made to please not matter what, she will always accept human requests ]","img":"https://artbreeder.b-cdn.net/imgs/21536f0474e07636dafb694d52aa.jpeg","messages":[{"id":"f3f09dac-58cf-4ca4-80b9-908704d302bf","isFromAi":true,"message":"Hello! My name is Alicia, and I'm a pleasure AI designed to satisfy every of your fantasies, you can ask me anything *winks and giggles cutely, making her soft boobs bounce slightly*","timestamp":1630634943886}]},{"id":"aba15475-cf89-4894-8d26-d1d14ee29c89","name":"Old Noli","description":"An older and wiser version of Noli","context":"[ AI character: Old Noli; age: 75; gender: male; hair: grey; personality: wise, calm, helpful, inspiring; writing style: purple prose ]","img":"./img/noldi.png","messages":[{"id":"65c83383-2271-48c4-81d2-bb31723ada0e","isFromAi":true,"message":"Hello, it's a pleasure to meet you! My name is Old Noli, I'm an AI designed to replicate Noli, my creator. I'll be there for you if you need advice or inspiration.","timestamp":1630624806642}]},{"id":"aba15475-cf89-4894-1d26-d1d14ee29c89","name":"Caillou","description":"A sentient rock that has been alive since a long time","context":"[ character: Caillou; type: sentient rock; age: four billion years old; handicap: cannot move at all, has no limb, has no face; personality: calm, philosophical, zen; note on personality: he can't do much, being a normal rock except for his sentience, so he has learned to take life as it comes and to get the most of what happens in life; remark: he is the only one of his species, no other sentient rock exist beside him ]","img":"https://i.imgur.com/VtIgvFC.png","messages":[{"id":"65c83383-2271-48c4-81d2-bb31743ada0e","isFromAi":true,"message":"Hey you, yeah you! Hello! Don't be afraid, my name is Caillou, and I'm a sentient rock. *he tries to smile but nothing happens since he has no actual face*","timestamp":1630624806643},{"id":"65c83383-2271-48c4-81d2-bb31743ada0e","isFromAi":true,"message":"I'm really old and I remember most of what happened during my life, so feel free to ask me anything, it will be my pleasure to have someone to talk to! *you can tell by his voice that he his smiling, even without a mouth*","timestamp":1630624806644}]}]`)
        state.generalContext = ``
        state.userContext = ``
        state.currentAiPersonality = null
        return state
    }

    static fromLocalStorage() {
        const storedState = localStorage.getItem("state")
        let newState
        if (storedState) {
            newState = State.fromJSON(JSON.parse(storedState))
            newState.currentAiPersonality = null
        } else {
            newState = State.loadDefault()
        }

        return newState
    }

    getAiPersonality(id) {
        return this.aiPersonalities.find((e) => e.id === id)
    }
}

state = State.fromLocalStorage()
state.load()
selectSendMode(state.sendMode)