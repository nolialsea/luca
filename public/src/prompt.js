function preparePrompt(finishMessage = false, retryMessage = false, stopId = null, reaction = false, appendLine = null) {
    if (!state.currentAiPersonality) throw new Error()


    const allMessages = JSON.parse(JSON.stringify(state.currentAiPersonality.messages))

    const allLearnedInfo = allMessages.filter(m => m.message.startsWith("[ learned")).map(m => m.message)

    const stopMessage = !stopId ? null : state.currentAiPersonality.messages.find((e) => e.id === stopId)
    let nextMessageIsFromAi = !stopMessage ? null : stopMessage.isFromAi
    if (stopMessage) {
        for (let i = allMessages.length - 1; i >= 0; i--) {
            if (allMessages[i].id === stopMessage.id) {
                allMessages.splice(finishMessage ? i + 1 : i)
                break
            }
        }
    }

    // Context
    let promptContext = state.generalContext ? (state.generalContext + '\n') : ''
    promptContext += state.userContext ? (state.userContext + '\n') : ''
    promptContext += state.currentAiPersonality.context ? (state.currentAiPersonality.context + '\n') : ''

    // learned info
    if (allLearnedInfo && allLearnedInfo.length > 0) {
        promptContext += allLearnedInfo.join("\n") + "\n"
    }


    // First messages of the bot
    let promptFirstMessages = ""
    for (let message of allMessages) {
        if (!message.isFromAi) break
        if (message.message.startsWith("[")) {
            promptFirstMessages += `${message.message}\n`
        } else {
            promptFirstMessages += `${state.currentAiPersonality.name}: ${message.message}\n`
        }
    }
    promptFirstMessages += `...\n`

    // Last messages
    let promptLastMessages = ""
    let contextTokensLength = gpt3encoder.encode(promptContext).length
    let firstMessagesTokensLength = gpt3encoder.encode(promptFirstMessages).length
    let enoughMemory = true
    for (let i = allMessages.length - 1; i >= 0; i--) {
        const message = allMessages[i]
        let messageLine
        if (message.message.startsWith("[")) {
            if (promptContext.includes(`${message.message}\n`)) {
                promptContext = promptContext.replace(`${message.message}\n`, "")
                contextTokensLength = gpt3encoder.encode(promptContext).length
            }
            messageLine = `${message.message}\n`
        } else {
            messageLine = `${message.isFromAi ? state.currentAiPersonality.name : username}: ${message.message}\n`
        }

        // If enough space left in prompt
        const tokensLeft = 2048 - 150 - contextTokensLength - firstMessagesTokensLength
        if (gpt3encoder.encode(messageLine + promptLastMessages).length < tokensLeft) {
            promptLastMessages = messageLine + promptLastMessages
        } else {
            enoughMemory = false
            break
        }
    }

    let finalPrompt = promptContext + (enoughMemory ? promptLastMessages : promptFirstMessages + promptLastMessages)


    // Finalize
    if (appendLine) {
        finalPrompt += appendLine
    } else if (!finishMessage && !stopMessage) {
        finalPrompt += `${state.currentAiPersonality.name}:`
    } else if (finishMessage) {
        // Remove last '\n'
        finalPrompt = finalPrompt.substr(0, finalPrompt.length - 1).trimEnd()
        if (reaction) {
            finalPrompt += " *"
        }
    } else if (retryMessage) {
        finalPrompt += `${nextMessageIsFromAi ? state.currentAiPersonality.name : username}:`
    }

    console.info("Prompt:")
    console.info(finalPrompt)
    console.info("Token count:", gpt3encoder.encode(finalPrompt).length)
    console.info("Line count:", finalPrompt.split("\n").length)
    console.info("Enough space in context for all the messages ? " + (enoughMemory ? "Yes" : "No"))
    return finalPrompt
}