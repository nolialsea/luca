const inbox = document.getElementById("inbox_chat")
const history = document.getElementById("msg_history")
const inputMessage = document.getElementById("input_message")

const inputEditPersonalityId = document.getElementById("inputEditPersonalityId")
const inputEditPersonalityName = document.getElementById("inputEditPersonalityName")
const inputEditPersonalityDescription = document.getElementById("inputEditPersonalityDescription")
const inputEditPersonalityContext = document.getElementById("inputEditPersonalityContext")
const inputEditPersonalityImage = document.getElementById("inputEditPersonalityImage")

const inputEditPersonalityTemperature = document.getElementById("inputEditPersonalityTemperature")
const inputEditPersonalityTopK = document.getElementById("inputEditPersonalityTopK")
const inputEditPersonalityTopP = document.getElementById("inputEditPersonalityTopP")
const inputEditPersonalityRepetitionPenalty = document.getElementById("inputEditPersonalityRepetitionPenalty")
const inputEditPersonalityRepetitionPenaltyRange = document.getElementById("inputEditPersonalityRepetitionPenaltyRange")
const inputEditPersonalityRepetitionPenaltySlope = document.getElementById("inputEditPersonalityRepetitionPenaltySlope")

const inputGeneralSettingsApiKey = document.getElementById("inputGeneralSettingsApiKey")
const inputGeneralSettingsUsername = document.getElementById("inputGeneralSettingsUsername")
const inputGeneralSettingsUserContext = document.getElementById("inputGeneralSettingsUserContext")
const inputGeneralSettingsGeneralContext = document.getElementById("inputGeneralSettingsGeneralContext")
const inputGeneralSettingsNewPersonality = document.getElementById("inputGeneralSettingsNewPersonality")
let state
let accessToken
