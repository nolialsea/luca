function updateMessageCount(aiPersonality) {
    const divCounter = document.getElementById(aiPersonality.id + "_messageCount")
    divCounter.innerText = aiPersonality.messages.length.toString()
}

function setActiveConversation(id) {
    const elem = document.getElementById(id)
    const elems = document.getElementsByClassName("active_chat")
    for (let elem1 of elems) {
        elem1.classList.remove('active_chat')
    }

    elem.classList.add('active_chat')
    history.innerHTML = ''

    const aiPersonality = state.getAiPersonality(id)
    state.currentAiPersonality = aiPersonality
    for (let message of aiPersonality.messages) {
        if (message.isFromAi) {
            history.append(getHistoryIncomingEntry(aiPersonality, message))
        } else {
            history.append(getHistoryOutgoingEntry(message))
        }
    }

    history.scrollTop = history.scrollHeight;
}