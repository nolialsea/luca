class Message {
    id
    isFromAi
    message
    timestamp

    constructor(message, isFromAi = false) {
        this.id = uuidv4()
        this.isFromAi = isFromAi
        this.message = message
        this.timestamp = Date.now()
    }

    static fromJSON(json, isFromAi = false) {
        const message = new Message(json.message, isFromAi ? true : json.isFromAi)
        message.id = json.id
        message.timestamp = json.timestamp
        return message
    }
}
