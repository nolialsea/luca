
async function generateAndDisplayResult(finishMessage = false, retryMessage = false, stopId = null) {
    if (!state.currentAiPersonality) return

    const aiGenerationParams = (
        finishMessage && !stopId
        && state.currentAiPersonality.messages[state.currentAiPersonality.messages.length - 1].message.startsWith("[")
    )
    || (finishMessage && stopId && document.getElementById(stopId + "_text").innerText.startsWith("["))
        ? PARAMS_DEFAULT_GENERATOR : undefined

    const r = await generate(
        accessToken,
        preparePrompt(finishMessage, retryMessage, stopId), aiGenerationParams)
    if (!r) throw new Error()
    const replyMessage = new Message(r.trim(), true)

    if (!finishMessage) {
        if (stopId !== null) {
            const message = state.currentAiPersonality.messages.find((e) => e.id === stopId)
            message.message = r.trimEnd()
            document.getElementById(stopId + "_text").innerText = r.trimEnd()
        } else {
            state.currentAiPersonality.messages.push(replyMessage)
            history.append(getHistoryIncomingEntry(state.currentAiPersonality, replyMessage))
        }
    } else {
        const lastMessage = state.currentAiPersonality.messages[state.currentAiPersonality.messages.length - 1]
        lastMessage.message += r.trimEnd()
        document.getElementById(lastMessage.id + "_text").innerText += r.trimEnd()
    }

    history.scrollTop = history.scrollHeight;

    saveState()
    updateMessageCount(state.currentAiPersonality)
}

async function sendMessageButton(event) {
    if (event && event.key !== "Enter") return
    if (!state) return

    switch (state.sendMode) {
        case "send":
            await sendMessage()
            break
        case "autocomplete":
            await autocompleteAndSendMessage()
            break
        case "autocompleteFromAi":
            await autocompleteAndSendMessageFromAi()
            break
        case "noAnswer":
            await sendMessageNoAnswer()
            break
    }
    history.scrollTop = history.scrollHeight;
}

async function sendMessage() {
    if (inputMessage.value?.trim()) {
        const message = new Message(inputMessage.value)
        history.append(getHistoryOutgoingEntry(message))
        inputMessage.value = ""
        state.currentAiPersonality.messages.push(message)
        history.scrollTop = history.scrollHeight;

        saveState()
        updateMessageCount(state.currentAiPersonality)
    }

    await generateAndDisplayResult()
}

async function sendMessageNoAnswer() {
    if (inputMessage.value?.trim()) {
        const message = new Message(inputMessage.value)
        history.append(getHistoryOutgoingEntry(message))
        inputMessage.value = ""
        state.currentAiPersonality.messages.push(message)
        history.scrollTop = history.scrollHeight;

        saveState()
        updateMessageCount(state.currentAiPersonality)
    }
}

async function autocompleteAndSendMessage() {
    const message = new Message(inputMessage.value)
    history.append(getHistoryOutgoingEntry(message))
    inputMessage.value = ""
    state.currentAiPersonality.messages.push(message)

    await continueMessage(null, state.currentAiPersonality.messages[state.currentAiPersonality.messages.length - 1].id)
    updateMessageCount(state.currentAiPersonality)
    saveState()
}

async function autocompleteAndSendMessageFromAi() {
    const message = new Message(inputMessage.value, true)
    history.append(getHistoryIncomingEntry(state.currentAiPersonality, message))
    inputMessage.value = ""
    state.currentAiPersonality.messages.push(message)

    await continueMessage(null, state.currentAiPersonality.messages[state.currentAiPersonality.messages.length - 1].id)
    updateMessageCount(state.currentAiPersonality)

    saveState()
}

function selectSendMode(sendMode) {
    if (!state) console.error("No state loaded!")

    const selectedButton = document.getElementById("selectedButton")
    let message
    let stateLabel

    switch (sendMode) {
        case "send":
            message = "Send"
            stateLabel = "send"
            break
        case "autocomplete":
            message = "Autocomplete and send"
            stateLabel = "autocomplete"
            break
        case "autocompleteFromAi":
            message = "Autocomplete and send from AI"
            stateLabel = "autocompleteFromAi"
            break
        case "noAnswer":
            message = "Send with no answer"
            stateLabel = "noAnswer"
            break
    }

    selectedButton.innerText = message
    state.sendMode = stateLabel
}

function retry() {
    if (!state.currentAiPersonality) return

    const p = state.currentAiPersonality.messages.pop()

    if (p) {
        history.lastChild.remove()
        generateAndDisplayResult().then(r => saveState())
    }
}

document.body.onkeydown = (e) => {
    if (e.ctrlKey && (e.code === "Enter" || e.code === "NumpadEnter")) {
        sendMessageButton().then(r => null)
    } else if (e.code === "KeyR" && e.altKey) {
        retry()
    } else if (e.code === "KeyF" && e.altKey) {
        e.preventDefault()
        generateAndDisplayResult(true).then(r => null)
    }
}
