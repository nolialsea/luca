function showModalGeneralSettings() {
    if (!state) return

    inputGeneralSettingsApiKey.value = localStorage.getItem("novelAiAccessToken")
    inputGeneralSettingsUsername.value = username
    inputGeneralSettingsUserContext.value = state.userContext
    inputGeneralSettingsUserContext.placeholder = `example: [ character: Noli; age: 30; gender: male ]`
    inputGeneralSettingsGeneralContext.value = state.generalContext
    inputGeneralSettingsGeneralContext.placeholder = `example: [ this is a private textual conversation between an AI and a human ]`
}

function saveGeneralSettings() {
    if (!state) return
    if (!inputGeneralSettingsApiKey.value
        || !inputGeneralSettingsUsername.value) {
        return
    }

    novelAiAccessToken = inputGeneralSettingsApiKey.value
    username = inputGeneralSettingsUsername.value
    state.userContext = inputGeneralSettingsUserContext.value || ""
    state.generalContext = inputGeneralSettingsGeneralContext.value || ""

    localStorage.setItem("novelAiAccessToken", novelAiAccessToken)
    localStorage.setItem("username", username)

    saveState()
    alert("Saved!")
}