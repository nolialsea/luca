const generateUnthrottled = (function () {
    return async (accessToken, input, params = PARAMS_DEFAULT) => {

        if (params !== PARAMS_DEFAULT) {
            console.log("Using generator settings")
        } else {
            // Injects custom AI generation settings
            if (state?.currentAiPersonality?.conf) {
                if (state.currentAiPersonality.conf.hasOwnProperty("temperature")) {
                    params.temperature = state.currentAiPersonality.conf.temperature
                }
                if (state.currentAiPersonality.conf.hasOwnProperty("top_k")) {
                    params.top_k = state.currentAiPersonality.conf.top_k
                }
                if (state.currentAiPersonality.conf.hasOwnProperty("top_p")) {
                    params.top_p = state.currentAiPersonality.conf.top_p
                }
                if (state.currentAiPersonality.conf.hasOwnProperty("repetition_penalty")) {
                    params.repetition_penalty = state.currentAiPersonality.conf.repetition_penalty
                }
                if (state.currentAiPersonality.conf.hasOwnProperty("repetition_penalty_range")) {
                    params.repetition_penalty_range = state.currentAiPersonality.conf.repetition_penalty_range
                }
                if (state.currentAiPersonality.conf.hasOwnProperty("repetition_penalty_slope")) {
                    params.repetition_penalty_slope = state.currentAiPersonality.conf.repetition_penalty_slope
                }
            }
        }

        const response = await fetch('/generate', {
            method: 'POST',
            body: JSON.stringify({input, model: "euterpe-v2" /*"6B-v4"*/, parameters: params, key: novelAiAccessToken}),
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': 'no-cors'
            }
        });

        const result = await response.json();
        return result.output
    }
})()

let lastGenerationTimestamp = Date.now()

// throttles generation at one request per second
const generate = function (accessToken, input, params) {
    return new Promise((resolve) => {
        const timeDiff = Date.now() - lastGenerationTimestamp
        lastGenerationTimestamp = Date.now()
        setTimeout(async () => {
            resolve(await generateUnthrottled(accessToken, input, params))
        }, timeDiff < 2000 ? 2000 - timeDiff : 0)
    })
}