function showModalEditPersonality() {
    if (!state || !state.currentAiPersonality) return

    inputEditPersonalityId.value = state.currentAiPersonality.id
    inputEditPersonalityName.value = state.currentAiPersonality.name
    inputEditPersonalityDescription.value = state.currentAiPersonality.description
    inputEditPersonalityDescription.placeholder = "Message shown in the contact list, purely for display"
    inputEditPersonalityContext.value = state.currentAiPersonality.context
    inputEditPersonalityContext.placeholder = "example: [ AI character: Alice; gender: female; hair: golden; personality: joyful, helpful, talkative; writing style: purple prose, descriptive body language ]"
    inputEditPersonalityImage.value = state.currentAiPersonality.img

    if (state.currentAiPersonality.conf) {
        if (state.currentAiPersonality.conf.temperature) inputEditPersonalityTemperature.value = state.currentAiPersonality.conf.temperature
        if (state.currentAiPersonality.conf.top_k) inputEditPersonalityTopK.value = state.currentAiPersonality.conf.top_k
        if (state.currentAiPersonality.conf.top_p) inputEditPersonalityTopP.value = state.currentAiPersonality.conf.top_p
        if (state.currentAiPersonality.conf.repetition_penalty) inputEditPersonalityRepetitionPenalty.value = state.currentAiPersonality.conf.repetition_penalty
        if (state.currentAiPersonality.conf.repetition_penalty_range) inputEditPersonalityRepetitionPenaltyRange.value = state.currentAiPersonality.conf.repetition_penalty_range
        if (state.currentAiPersonality.conf.repetition_penalty_slope) inputEditPersonalityRepetitionPenaltySlope.value = state.currentAiPersonality.conf.repetition_penalty_slope
    }
}

function saveEditPersonality() {
    if (!state) return
    if (!inputEditPersonalityName.value
        || !inputEditPersonalityDescription.value
        || !inputEditPersonalityContext.value
        || !inputEditPersonalityImage.value
        || !inputEditPersonalityId.value) {
        return
    }

    const conf = {}
    if (inputEditPersonalityTemperature.value) {
        conf.temperature = parseFloat(inputEditPersonalityTemperature.value)
    }
    if (inputEditPersonalityTopK.value) {
        conf.top_k = parseInt(inputEditPersonalityTopK.value)
    }
    if (inputEditPersonalityTopP.value) {
        conf.top_p = parseFloat(inputEditPersonalityTopP.value)
    }
    if (inputEditPersonalityRepetitionPenalty.value) {
        conf.repetition_penalty = parseFloat(inputEditPersonalityRepetitionPenalty.value)
    }
    if (inputEditPersonalityRepetitionPenaltyRange.value) {
        conf.repetition_penalty_range = parseInt(inputEditPersonalityRepetitionPenaltyRange.value)
    }
    if (inputEditPersonalityRepetitionPenaltySlope.value) {
        conf.repetition_penalty_slope = parseFloat(inputEditPersonalityRepetitionPenaltySlope.value)
    }

    let previouslySelectedAi
    if (!state.currentAiPersonality) {
        const personality = new AIPersonality(
            inputEditPersonalityName.value,
            inputEditPersonalityDescription.value,
            inputEditPersonalityContext.value,
            inputEditPersonalityImage.value,
            inputEditPersonalityId.value
        )
        personality.conf = conf

        state.aiPersonalities.push(personality)
        previouslySelectedAi = personality.id
    } else {
        state.currentAiPersonality.id = inputEditPersonalityId.value
        state.currentAiPersonality.name = inputEditPersonalityName.value
        state.currentAiPersonality.description = inputEditPersonalityDescription.value
        state.currentAiPersonality.img = inputEditPersonalityImage.value
        state.currentAiPersonality.context = inputEditPersonalityContext.value

        state.currentAiPersonality.conf = conf
        previouslySelectedAi = state.currentAiPersonality.id
    }

    saveState()
    state.load()
    setActiveConversation(previouslySelectedAi)

    alert("Saved!")
}

function download(filename, text) {
    const element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function downloadConversationAsTextFile() {
    if (!state) {
        alert("Error: No state found")
        return
    }

    if (!state.currentAiPersonality) {
        alert("You need to select an AI personality first")
        return
    }

    let text = "## Context\n"

    // Context
    text += state.generalContext ? (state.generalContext + '\n') : ''
    text += state.userContext ? (state.userContext + '\n') : ''
    text += state.currentAiPersonality.context ? (state.currentAiPersonality.context + '\n') : ''

    text += "## Story\n"

    for (let message of state.currentAiPersonality.messages) {
        if (message.message.startsWith("[")) {
            text += `${message.message}\n`
        } else {
            text += `${message.isFromAi ? state.currentAiPersonality.name : username}: ${message.message}\n`
        }
    }

    download(`${state.currentAiPersonality.name}_${Date.now()}.txt`, text)
}