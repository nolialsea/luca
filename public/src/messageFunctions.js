function deleteMessage(id) {
    state.currentAiPersonality.messages = state.currentAiPersonality.messages.filter(e => e.id !== id)
    document.getElementById(id).remove()
    saveState()
    updateMessageCount(state.currentAiPersonality)
}

function deleteMessageUpToHere(id) {
    if (!confirm("Are you sure you want to delete all the messages up to this one included?")) return

    let stop = false
    state.currentAiPersonality.messages = state.currentAiPersonality.messages.filter(e => {
        if (e.id === id) {
            stop = true
        }
        return !stop
    })
    updateMessageCount(state.currentAiPersonality)
    const previouslySelectedAi = state.currentAiPersonality.id
    state.load()
    setActiveConversation(previouslySelectedAi)
    saveState()
}

function editMessage(elem, id, confirmEdition = false) {
    const textSpan = document.getElementById(id + '_text')
    const textInput = document.getElementById(id + '_textInput')

    if (!confirmEdition && textInput.hidden) {
        textInput.value = textSpan.innerText
        textSpan.hidden = true
        textInput.hidden = false
    } else {
        if (confirmEdition && elem.key !== "Enter" && elem.key !== "Escape") return
        const text = textInput.value
        if (elem.key === "Enter" || !confirmEdition) {
            textSpan.innerText = text
            state.currentAiPersonality.messages.find((m) => m.id === id).message = text
        } else {
            textInput.value = textSpan.innerText
        }
        textSpan.hidden = false
        textInput.hidden = true
        saveState()
        history.scrollTop = history.scrollHeight;
    }
}

async function retryMessage(elem, id) {
    const messageInMemory = state.currentAiPersonality.messages.find((m) => m.id === id)
    const textSpan = document.getElementById(id + '_text')
    const spinner = document.getElementById(id + '_spinner')
    spinner.hidden = false

    textSpan.innerText = ""
    const prompt = preparePrompt(false, true, id)
    textSpan.innerText = (await generate(accessToken, prompt)).trim()

    messageInMemory.message = textSpan.innerText.trim()

    saveState()
    history.scrollTop = history.scrollHeight;
    spinner.hidden = true
}

async function retryMessageFromInput(elem, id){
    const messageInMemory = state.currentAiPersonality.messages.find((m) => m.id === id)
    const textSpan = document.getElementById(id + '_text')
    const inputSpan = document.getElementById(id + '_textInput')
    const spinner = document.getElementById(id + '_spinner')
    spinner.hidden = false

    textSpan.innerText = inputSpan.value
    messageInMemory.message = inputSpan.value
    const prompt = preparePrompt(true, false, id)
    const result = await generate(accessToken, prompt, textSpan.innerText.startsWith("[") ? PARAMS_DEFAULT_GENERATOR : undefined)
    textSpan.innerText += result.trimEnd()

    state.currentAiPersonality.messages.find((m) => m.id === id).message = textSpan.innerText.trim()

    saveState()
    history.scrollTop = history.scrollHeight;
    spinner.hidden = true
}

async function continueMessage(elem, id) {
    const textSpan = document.getElementById(id + '_text')
    const spinner = document.getElementById(id + '_spinner')
    spinner.hidden = false

    const prompt = preparePrompt(true, false, id)
    const result = await generate(accessToken, prompt, textSpan.innerText.startsWith("[") ? PARAMS_DEFAULT_GENERATOR : undefined)
    textSpan.innerText += result.trimEnd()

    state.currentAiPersonality.messages.find((m) => m.id === id).message = textSpan.innerText.trim()

    saveState()
    history.scrollTop = history.scrollHeight;
    spinner.hidden = true
}

async function addReactionToMessage(elem, id) {
    const textSpan = document.getElementById(id + '_text')
    const spinner = document.getElementById(id + '_spinner')
    spinner.hidden = false
    const messageInMemory = state.currentAiPersonality.messages.find((m) => m.id === id)

    if (textSpan.innerText.includes("*")) {
        const newMessage = textSpan.innerText.replace(/\*[^\n]*/i, "").trim()
        textSpan.innerText = newMessage
        messageInMemory.message = newMessage
    }

    const prompt = preparePrompt(true, false, id, true)
    const result = await generate(accessToken, prompt)
    textSpan.innerText += " *" + result.replace(/\*[^\n]*/i, "*").trimEnd()

    messageInMemory.message = textSpan.innerText.trim()

    saveState()
    history.scrollTop = history.scrollHeight;
    spinner.hidden = true
}

async function memorizeMessages(id) {
    const name = document.getElementById(id + "_name").innerText
    const spinner = document.getElementById(id + '_spinner')
    spinner.hidden = false
    const isFromAi = name === state.currentAiPersonality.name
    const promptToSend = preparePrompt(true, false, id, false, `[ new info learned about ${name}:`)
    const result = await generate(accessToken, promptToSend, PARAMS_DEFAULT)

    const newContext = (`[ learned about ${name}:` + result.replace(/ ][^\n]*/, " ]")).trim()
    if (confirm(`New context line:\n\n${newContext}\n\nDo you want to append this line to the current context?`)) {
        const message = new Message(newContext, !isFromAi)
        if (isFromAi) {
            history.append(getHistoryOutgoingEntry(message))
        } else {
            history.append(getHistoryIncomingEntry(state.currentAiPersonality, message))
        }
        state.currentAiPersonality.messages.push(message)
        saveState()
        history.scrollTop = history.scrollHeight;
    }
    spinner.hidden = true
}

