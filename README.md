# Light UI Client for ALICE (or LUCA for short)

## REQUIREMENTS
A paid NovelAI account to use their AI API

## USAGE
When the page opens, it will be empty from conversations and you'll have to load the save file of your choice (this is a temporary solution)  
At the bottom of the screen are two buttons, one to load a save file, one to download the new, updated save  
**Don't forget to download the save before closing the window, or you'll love your conversations**

Some commands:
  - force the AI to send a message: Press <enter> with no input
  - force the AI to finish last message: alt+F
  - force the AI to rewrite last message: alt+R