import axios from "axios"
import express from "express"
import cors from "cors";

const getNovelAIBearerToken = async (key) => {
    return new Promise((resolve, reject) => {
        axios.post("https://api.novelai.net/user/login", {key: key}, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(r => {
                resolve(r.data.accessToken)
            })
            .catch(err => {
                reject(err)
            })
    })
}

class AiService {
    static cacheBearerTokens = {}

    static async getBearer(key) {
        if (this.cacheBearerTokens[key]) {
            return this.cacheBearerTokens[key]
        }
        try {
            this.cacheBearerTokens[key] = await getNovelAIBearerToken(key)
            return this.cacheBearerTokens[key]
        } catch (e) {
            console.error(e)
        }
    }

    static async generate(key, prompt, model, params) {
        const backendURL = "https://api.novelai.net/ai"
        let res
        try {
            res = await axios.post(
                backendURL + "/generate",
                {
                    input: prompt,
                    model,
                    parameters: params
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer " + await this.getBearer(key)
                    }
                }
            )
        } catch (e) {
            console.error("An error happened with the custom proxy", e)
            res = null
        }

        return res?.data
    }
}


const app = express()
app.use(express.json())
app.use(cors({
    origin: '*'
}))
app.use(express.urlencoded({extended: true}))

app.post('/api/v1/test', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.json('{"status": "SUCCESS"}')
})

app.post('/generate', async function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')

    AiService.generate(
        req.body.key,
        req.body.input,
        req.body.model,
        req.body.parameters
    ).then(r => res.json(r))
})

app.post('/validatekey', async function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')

    const bearer = await AiService.getBearer(req.body.key)

    if (bearer){
        res.sendStatus(200)
    }else{
        throw new Error()
    }
})

app.listen(parseInt("4679"))